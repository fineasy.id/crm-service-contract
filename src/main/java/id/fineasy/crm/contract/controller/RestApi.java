package id.fineasy.crm.contract.controller;

import id.fineasy.crm.contract.component.DataUtils;
import id.fineasy.crm.contract.worker.MessageSender;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_DEDUCTION_CONTRACT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_RECALCULATE_CONTRACT_ALL;

@RestController
public class RestApi {
    @Autowired
    MessageSender messageSender;

    @RequestMapping(value = {"/new"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> newContract(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;

        // put your fuckin process here!
        long appId = Long.parseLong(request.getParameter("app_id"));
        JSONObject jsonReq = new JSONObject();
        jsonReq.put("appId", appId);

        messageSender.newContract(jsonReq);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }

    @RequestMapping(value = {"/activate"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> activate(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;

        Long contractId = Long.parseLong(request.getParameter("contract_id"));
        Long activateBy = Long.parseLong(request.getParameter("activate_by"));
        messageSender.activateContract(contractId, activateBy);

        jsonResult.put("contractId", contractId);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }

    @RequestMapping(value = {"/activate/{id}"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> activate2(HttpServletRequest request, @PathVariable("id") Long contractId) {
        JSONObject jsonResult = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;

        Long activateBy = 1L;
        messageSender.activateContract(contractId, activateBy);

        jsonResult.put("contractId", contractId);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }

    @RequestMapping(value = {"/recalculate/{id}"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> recalculate(HttpServletRequest request, @PathVariable("id") String id) {
        JSONObject jsonResult = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;

        if (id.equals("all")) {
            messageSender.send(QUEUE_RECALCULATE_CONTRACT_ALL, jsonResult);
        } else {
            Long contractId = Long.parseLong(id);
            messageSender.recalculateContract(contractId);
            jsonResult.put("contractId", contractId);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }

    @RequestMapping(value = {"/deduction/{id}"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> deduction(HttpServletRequest request, @PathVariable("id") String id) {
        JSONObject jsonResult = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;

        Long contractId = Long.parseLong(id);
        messageSender.send(QUEUE_DEDUCTION_CONTRACT, new JSONObject().put("contractId", contractId));

        jsonResult.put("contractId", contractId);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }
}
