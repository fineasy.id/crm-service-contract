package id.fineasy.crm.contract.component;

import id.fineasy.crm.contract.worker.MessageSender;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import static id.fineasy.crm.contract.component.DataUtils.*;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_SMS;

public class ContractDeductionUtils {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    Long contractId;
    JSONObject contractDetail;
    private static final String dateFormat = "yyyy-MM-dd";
    final DateTimeFormatter dtf = DateTimeFormat.forPattern(dateFormat);

    Long prevBalance = 0L;
    Integer dpd = 0;
    Long deductionAmount;
    LocalDate today;
    LocalDate startDate;
    LocalDate endDate;
    Long totalDebt;
    boolean repaid = false;
    int contractStatusId = STATUS_CONTRACT_ACTIVE;

    private final MessageSender messageSender;
    private final SmsUtils smsUtils;
    private final Long contactId;

    DataUtils dataUtils;
    final ArrayList<Integer> closedStatuses = new ArrayList<>();
    LocalDate lastProcessedDate;

    public ContractDeductionUtils(long contractId, DataUtils dataUtils, MessageSender messageSender, SmsUtils smsUtils) throws Exception {
        this.smsUtils = smsUtils;
        this.messageSender = messageSender;
        repaid = false;
        this.dataUtils = dataUtils;
        closedStatuses.add(STATUS_CONTRACT_CLOSED);
        closedStatuses.add(STATUS_CONTRACT_OVERPAYMENT);
        contractDetail = dataUtils.getContractDetail(contractId);
        contactId = contractDetail.getLong("contact_id");
    }

    public void run() throws Exception {
        this.contractId = contractId;
        log.info("Start processing deduction request: "+contractId);
        deductionAmount = contractDetail.getLong("daily_charge");
        today = LocalDate.now();

        startDate = LocalDate.parse(contractDetail.getString("contract_charged_on"), dtf);
        endDate = LocalDate.parse(contractDetail.getString("contract_end_on"), dtf);
        totalDebt = contractDetail.getLong("total_debt");

        log.info("Today is: "+today);

        TransactionTemplate transactionTemplate = new TransactionTemplate(dataUtils.getTrx());
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                try {
                    int days = Days.daysBetween(startDate, today).getDays();
                    log.info("Start deduct: "+startDate+", until today: "+today+", number of days : "+days);

                    resetContractDeduction();

                    for (int i=0;i<=days;i++) {
                        processDeduction(startDate.plusDays(i), i+1);
                        if (repaid) break;
                    }
                    updateContractStatus();
                    updateContactDpd();
                    if (repaid) {
                        updateClosedContact();
                    }
                } catch (Exception e) {
                    transactionStatus.setRollbackOnly();
                    log.error("Error deduction: "+e,e);
                }
            }
        });
    }

    private void processDeduction(LocalDate date, final int seq) {
        if (repaid) {
            log.info("Contract already repaid ... skipping!");
            return;
        }
        log.info("Processing deduction contract #"+contractId+": "+date.toString(dateFormat));
        Long repayment = getRepaymentAmount(date);

        Long amount = deductionAmount;
        if (date.isAfter(endDate)) {
            amount = 0L;
        }

        Long payable = (amount + prevBalance) - repayment;

        if (seq > 0 && prevBalance>0) dpd+=1;
        else dpd = 0;


        // check if no more payable ...
        Long totalRepayment = getTotalRepayment(date);
        Long debtRemaining = totalDebt - totalRepayment;

        log.info("Total repayment: "+totalRepayment);

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractId", contractDetail.getLong("contract_id"));
        params.addValue("seq", seq);
        params.addValue("deductOn", date.toString(dateFormat));
        params.addValue("amount", amount);
        params.addValue("prevBalance", prevBalance);
        params.addValue("repayment", repayment);
        params.addValue("payable", (payable<0)?0:payable);
        params.addValue("dpd", dpd);
        params.addValue("totalRepayment", totalRepayment);
        params.addValue("debtRemaining", debtRemaining);

        String sql = " INSERT INTO contract_deduction (seq,contract_id,deduct_on,amount,prev_balance,repayment,payable,dpd,total_repayment,debt_remaining) " +
                " VALUES (:seq,:contractId,:deductOn,:amount,:prevBalance,:repayment,:payable,:dpd,:totalRepayment,:debtRemaining)  " +
                " ON DUPLICATE KEY UPDATE amount=:amount,prev_balance=:prevBalance, repayment=:repayment, payable=:payable, dpd=:dpd, total_repayment=:totalRepayment,debt_remaining=:debtRemaining ";

        dataUtils.getJT().update(sql, params);

        updateContract(dpd, (payable<0)?0:payable, totalRepayment, debtRemaining);

        prevBalance = payable;

        if (debtRemaining<=0) {
            repaid = true;
            log.info("Contract is RE-PAID!");
            if (debtRemaining < 0) contractStatusId = STATUS_CONTRACT_OVERPAYMENT;
            else contractStatusId = STATUS_CONTRACT_CLOSED;

        }
        else if (date.isAfter(endDate) && debtRemaining>0) {
            contractStatusId = STATUS_CONTRACT_OVERDUE;
        }
        lastProcessedDate = date;

    }

    private void closeContract() {

    }

    private void updateContactDpd() {
        log.info("Update contact DPD!");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contactId", contactId);

        String sql = " UPDATE contact c SET " +
                " dpd_sum=IFNULL((SELECT SUM(dpd_max) FROM contract t JOIN application a ON (a.app_id=t.app_id) WHERE a.contact_id=c.contact_id),0)," +
                " dpd_max=IFNULL((SELECT MAX(dpd_max) FROM contract t JOIN application a ON (a.app_id=t.app_id) WHERE a.contact_id=c.contact_id),0) " +
                " WHERE c.contact_id=:contactId ";

        dataUtils.getJT().update(sql, params);
    }

    private void updateContractStatus() throws Exception {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractId", contractDetail.getLong("contract_id"));
        params.addValue("statusId", contractStatusId);
        String sql = " UPDATE contract SET status_id=:statusId WHERE contract_id=:contractId ";
        dataUtils.getJT().update(sql, params);
    }

    private void updateClosedContact() throws Exception {
        log.info("Closing the contract #"+contractId);
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("contractId", contractDetail.getLong("contract_id"));
        params.addValue("closedOn", lastProcessedDate.toString("yyyy-MM-dd"));

        String sql = " UPDATE contract SET closed_on=:closedOn WHERE contract_id=:contractId ";
        dataUtils.getJT().update(sql, params);

        String smsText = smsUtils.getMessageTemplate("FULL_REPAYMENT2");
        if (dataUtils.repeatEligible(contactId)) {
            smsText = smsUtils.getMessageTemplate("FULL_REPAYMENT1");
        }

        log.info("Sending thank you SMS!");

        JSONObject jsonReq = new JSONObject();
        jsonReq.put("mobile", contractDetail.getString("mobile"));
        jsonReq.put("content", smsText);
        jsonReq.put("tag", "SYSTEM_ALERT");
        jsonReq.put("ref", UUID.randomUUID().toString());
        messageSender.send(QUEUE_SMS, jsonReq);
    }

    private Long getRepaymentAmount(LocalDate date) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractId", contractDetail.getLong("contract_id"));
        params.addValue("typeId", TYPE_CASHFLOW_INFLOW);
        params.addValue("categoryId", CATEGORY_CASHFLOW_REPAYMENT);
        params.addValue("statusId", STATUS_CASHFLOW_COMPLETED);
        params.addValue("trxDate", date.toString(dateFormat));

        String sql = " SELECT IFNULL(SUM(amount),0) FROM cashflow WHERE contract_id=:contractId AND type_id=:typeId AND category_id=:categoryId AND status_id=:statusId AND trx_date=:trxDate ";
        return dataUtils.getJT().queryForObject(sql, params, Long.class);
    }

    private Long getTotalRepayment(LocalDate date) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractId", contractDetail.getLong("contract_id"));
        params.addValue("typeId", TYPE_CASHFLOW_INFLOW);
        params.addValue("categoryId", CATEGORY_CASHFLOW_REPAYMENT);
        params.addValue("statusId", STATUS_CASHFLOW_COMPLETED);
        params.addValue("trxDate", date.toString(dateFormat));

        String sql = " SELECT IFNULL(SUM(amount),0) FROM cashflow WHERE contract_id=:contractId AND type_id=:typeId AND category_id=:categoryId AND status_id=:statusId AND trx_date<=:trxDate ";
        return dataUtils.getJT().queryForObject(sql, params, Long.class);
    }

    private void resetContractDeduction() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractId", contractDetail.getLong("contract_id"));
        params.addValue("chargedOn", contractDetail.getString("contract_charged_on"));
        String sql = " DELETE FROM contract_deduction WHERE contract_id=:contractId ";
        dataUtils.getJT().update(sql, params);

        sql = " UPDATE contract SET dpd_current=0, dpd_max=0, closed_on=NULL WHERE contract_id=:contractId ";
        dataUtils.getJT().update(sql, params);
    }

    private void updateContract(int dpd, long payable, long totalRepayment, long debtRemaining) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("contractId", contractDetail.getLong("contract_id"));
        params.addValue("dpd", dpd);
        params.addValue("payable", payable);
        params.addValue("totalRepayment", totalRepayment);
        params.addValue("debtRemaining", debtRemaining);

        String sql = " UPDATE contract SET dpd_current=:dpd, payable=:payable, total_repayment=:totalRepayment, debt_remaining=:debtRemaining WHERE contract_id=:contractId ";
        dataUtils.getJT().update(sql, params);
    }
}
