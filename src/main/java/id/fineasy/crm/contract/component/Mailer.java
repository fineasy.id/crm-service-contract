package id.fineasy.crm.contract.component;

import com.netflix.discovery.converters.Auto;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class Mailer  {
    @Autowired
    public JavaMailSender mailSender;

    @Autowired
    private DataUtils dataUtils;

    @Value("${spring.mail.from}")
    private String from;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public void send(String to, String subject, String htmlContent, List<FileAttachment> attachments) throws Exception {
        MimeMessage mailMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true);

        helper.setTo(to);
        helper.setFrom(from);
        helper.setBcc(from);
        helper.setSubject(subject);
        helper.setText(htmlContent, true);

        if (attachments!=null) {
            for (FileAttachment attachment : attachments ) {
                helper.addAttachment(attachment.getName(), attachment.getFileStream(), attachment.getMimeType());
            }
        }

        send(mailMessage);
    }

    public void send(List<String> to, String subject, String htmlContent, List<File> attachments) throws Exception {
        MimeMessage mailMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true);

        String[] toList = new String[to.size()];
        toList = to.toArray(toList);

        helper.setTo(toList);
        helper.setBcc(from);
        helper.setFrom(from);
        helper.setSubject(subject);
        helper.setText(htmlContent, true);

        if (attachments!=null) {
            for (File attachment : attachments ) {
                helper.addAttachment(attachment.getName(),attachment);
            }
        }
        send(mailMessage);
    }

    public void send(String to, String subject, String htmlContent, File attachment) throws Exception {
        ArrayList<String> tos = new ArrayList<>();
        tos.add(to);
        ArrayList<File> attachments = new ArrayList<>();
        attachments.add(attachment);
        send(tos, subject, htmlContent, attachments);
    }

    private void send(MimeMessage mailMessage) {
        boolean retry = false;
        int retryCount = 0;
        do {
            try {
                retry = false;
                mailSender.send(mailMessage);
                log.info("Email sent!");
            } catch (Exception e) {
                log.error("Error sending email: "+e,e);
                if (retryCount < 3) {
                    log.info("Retrying ...");
                    retry  = true;
                    retryCount++;
                }
            }
        } while(retry);
    }

    public void sendLogger(String subject, String htmlContent) throws Exception {
        MapSqlParameterSource params = new MapSqlParameterSource();
        String sql = " SELECT cfg_val FROM config WHERE cfg_key like 'system.monitor.mail.to%' ";
        List<String> recipients = dataUtils.getJT().queryForList(sql, params, String.class);

        subject = "[FINEASY-SYSTEM-MONITOR] "+subject;
        String body = htmlContent;
        body+="<p>Generated at "+DateTime.now().toString("yyyy-MM-dd HH:mm:ss")+"</p>";
        List<File> attachments = new ArrayList<>();
        send(recipients,subject,body,attachments);
    }

    public static class FileAttachment {
        private final String name;
        private final ByteArrayResource fileStream;
        private final String mimeType;

        public FileAttachment(String name, ByteArrayResource fileStream, String mimeType) {
            this.name = name;
            this.fileStream = fileStream;
            this.mimeType = mimeType;
        }

        public String getName() {
            return name;
        }

        public String getMimeType() {
            return mimeType;
        }

        public ByteArrayResource getFileStream() {
            return fileStream;
        }
    }
}
