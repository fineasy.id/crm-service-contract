package id.fineasy.crm.contract.component;

import com.sun.star.beans.PropertyValue;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XDesktop;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.text.XTextDocument;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import com.sun.star.util.CloseVetoException;
import com.sun.star.util.XCloseable;
import com.sun.star.util.XReplaceDescriptor;
import com.sun.star.util.XReplaceable;
import ooo.connector.BootstrapSocketConnector;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
public class DocumentUtils {
    @Autowired
    ApplicationContext mContext;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    ConfigUtils configUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public File createPrintableDoc(Long contractId) throws Exception {

        File contractFile = new File(configUtils.getConfig("contract.folder")+"/contract-"+String.format("%06d", contractId)+".pdf");

        boolean retry = false;
        int retryCount = 0;

        do {
            retry = false;
            try {
                JSONObject contractDetail = dataUtils.getContractDetail(contractId);
                File srcFile = new File(configUtils.getConfig("agreement.mca.template"));

                log.info("Current operating system: "+System.getProperty("os.name"));
                String officeFolder = null;
                if (System.getProperty("os.name").contains("Windows"))
                    officeFolder = configUtils.getConfig("libreoffice.folder.windows");
                else
                    officeFolder = configUtils.getConfig("libreoffice.folder.linux");

                XComponentContext xContext = BootstrapSocketConnector.bootstrap(officeFolder);
                XMultiComponentFactory xMCF = xContext.getServiceManager();
                Object oDesktop = xMCF.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);
                XDesktop xDesktop = (XDesktop) UnoRuntime.queryInterface(XDesktop.class, oDesktop);

                String workingDir = "/tmp/";
                String sUrl = "file:///"+srcFile.getAbsolutePath().replaceAll("\\\\","/");
                log.info("File URL: "+sUrl);

                XComponentLoader xCompLoader = (XComponentLoader) UnoRuntime.queryInterface(com.sun.star.frame.XComponentLoader.class, xDesktop);

                PropertyValue[] propertyValues = new PropertyValue[0];

                propertyValues = new PropertyValue[1];
                propertyValues[0] = new PropertyValue();
                propertyValues[0].Name = "Hidden";
                propertyValues[0].Value = new Boolean(true);

                XComponent xComp = xCompLoader.loadComponentFromURL(sUrl, "_blank", 0, propertyValues);

                // Search and replace
                XReplaceDescriptor xReplaceDescr = null;
                XReplaceable xReplaceable = null;

                XTextDocument xTextDocument = (XTextDocument) UnoRuntime.queryInterface(XTextDocument.class, xComp);
                xReplaceable = (XReplaceable) UnoRuntime.queryInterface(XReplaceable.class, xTextDocument);
                xReplaceDescr = (XReplaceDescriptor) xReplaceable.createReplaceDescriptor();

                Map<String,Object> textParams = dataUtils.contractToTextParams(contractDetail);
                for (String key: textParams.keySet()) {
                    // mail merge the date
                    xReplaceDescr.setSearchString("{"+key+"}");
                    xReplaceDescr.setReplaceString(""+textParams.get(key));
                    xReplaceable.replaceAll(xReplaceDescr);
                }

                XStorable xStorable = (XStorable) UnoRuntime.queryInterface(XStorable.class, xComp);

                propertyValues = new PropertyValue[2];
                propertyValues[0] = new PropertyValue();
                propertyValues[0].Name = "Overwrite";
                propertyValues[0].Value = new Boolean(true);
                propertyValues[1] = new PropertyValue();
                propertyValues[1].Name = "FilterName";
                propertyValues[1].Value = "writer_pdf_Export";

                String pdfFile = "file:///"+contractFile.getAbsolutePath().replaceAll("\\\\","/");
                xStorable.storeToURL(pdfFile, propertyValues);

                log.info("PDF "+pdfFile+" created successfuly");

                try {
                    XCloseable xCloseable = (XCloseable) UnoRuntime.queryInterface(XCloseable.class, xComp);
                    xCloseable.close(true);
                    //storeContractFile(contractFile, contractDetail, "contract-customer");
                } catch (CloseVetoException ex) {
                    ex.printStackTrace();
                }
                xDesktop.terminate();
                retry = false;
            } catch (Exception e) {
                log.error("Error creating PDF contract :"+e+"... retrying ...",e);
                retryCount++;
                if (retryCount<3) {
                    retry = true;
                } else {
                    Utils.sendError(mContext, "Error creating printable (PDF) contract #"+contractId,e);
                    throw e;
                }
            }
        } while (retry);

        return contractFile;
    }
}
