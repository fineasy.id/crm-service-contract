package id.fineasy.crm.contract.component;

import com.mysql.cj.core.MysqlType;
import com.mysql.cj.jdbc.Blob;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XDesktop;
import com.sun.star.frame.XStorable;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import com.sun.star.util.CloseVetoException;
import com.sun.star.util.XCloseable;
import ooo.connector.BootstrapSocketConnector;
import com.sun.star.beans.PropertyValue;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.tika.Tika;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;
import java.io.*;
import java.lang.invoke.MethodHandles;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Types;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.LogManager;
import com.sun.star.comp.helper.Bootstrap;

import static id.fineasy.crm.contract.component.DataUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Component
public class ContractUtils {

    @Autowired
    ConfigUtils configUtils;

    @Autowired
    DataUtils dataUtils;


    @Autowired
    DocumentUtils documentUtils;


    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());



}
