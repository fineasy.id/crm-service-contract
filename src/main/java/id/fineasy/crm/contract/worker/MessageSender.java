package id.fineasy.crm.contract.worker;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;

import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_ACTIVATE_CONTRACT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_NEW_CONTRACT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_RECALCULATE_CONTRACT;

@Service
public class MessageSender {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private JmsTemplate jmsTemplate;

    public void newContract(JSONObject jsonApprovedApplication) {
        log.info("New contract request received! "+jsonApprovedApplication.toString());
        jmsTemplate.convertAndSend(QUEUE_NEW_CONTRACT, jsonApprovedApplication.toString());
    }

    public void activateContract(Long contractId, Long activateBy) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("contractId", contractId);
        jsonObject.put("activateBy", activateBy);
        log.info("Activate contract request received! "+jsonObject.toString());
        jmsTemplate.convertAndSend(QUEUE_ACTIVATE_CONTRACT, jsonObject.toString());

    }

    public void recalculateContract(Long contractId) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("contractId", contractId);
        jmsTemplate.convertAndSend(QUEUE_RECALCULATE_CONTRACT, jsonObject.toString());
    }

    public void send(String destination, JSONObject jsonReq) {
        jmsTemplate.convertAndSend(destination, jsonReq.toString());
    }
}
