package id.fineasy.crm.contract.worker;

import id.fineasy.crm.contract.component.ContractUtils;
import id.fineasy.crm.contract.component.DataUtils;
import id.fineasy.crm.contract.component.Mailer;
import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static id.fineasy.crm.contract.component.DataUtils.STATUS_CONTRACT_ACTIVE;
import static id.fineasy.crm.contract.component.DataUtils.STATUS_CONTRACT_OVERDUE;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_RECALCULATE_CONTRACT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_RECALCULATE_CONTRACT_ALL;

@Component
public class ContractRecalculationAllActive {
    @Autowired
    DataUtils dataUtils;
    @Autowired
    ContractUtils contractUtils;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MessageSender messageSender;
    @Autowired
    Mailer mailer;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Scheduled(cron = "1 1 0 * * *")
    public void test() {
        log.info("Crontab ContractRecalculationAllActive running ....");
        messageSender.send(QUEUE_RECALCULATE_CONTRACT_ALL, new JSONObject());
    }

    @JmsListener(destination = QUEUE_RECALCULATE_CONTRACT_ALL)
    public void recalculateContractAll(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            JSONObject jsonObject = new JSONObject(jsonObjectString);
            log.info("Recalculate all contracts request received! "+jsonObject.toString());
            ContractRecalculationAllActiveWorker worker = new ContractRecalculationAllActiveWorker();
            worker.run();
        } catch (Exception e) {
            log.error("Error start async contract all recalculation: "+e,e);
        }
    }

    private class ContractRecalculationAllActiveWorker {
        public void run() throws Exception {

            List<String> statusArray = new ArrayList<>();
            statusArray.add(""+STATUS_CONTRACT_ACTIVE);
            statusArray.add(""+STATUS_CONTRACT_OVERDUE);

            String statusIds = String.join(",", statusArray);

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("isActive", 1);

            String sql = " SELECT contract_id FROM _contract WHERE is_active=:isActive AND status_id IN ("+statusIds+") AND contract_charged_on <= CURRENT_DATE() ";
            List<Long> contractIds = dataUtils.getJT().queryForList(sql, params, Long.class);

            log.info("Found "+contractIds.size()+" contracts to be recalculated!");

            String loggerContent = "<p>Contract recalculation log:</p><ol>";

            for (Long contractId : contractIds) {
                loggerContent+="<li>Recalculating #"+contractId+"</li>";
                log.info("Recalculating #"+contractId);
                messageSender.send(QUEUE_RECALCULATE_CONTRACT, new JSONObject().put("contractId", contractId));
            }

            loggerContent+= "</ol>";
            mailer.sendLogger("Recalculation all contract activity "+ LocalDate.now().toString("yyyy-MM-dd"), loggerContent);
        }
    }
}
