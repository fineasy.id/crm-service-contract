package id.fineasy.crm.contract.worker;

import id.fineasy.crm.contract.component.*;
import org.joda.time.LocalDate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;

import static id.fineasy.crm.contract.component.DataUtils.*;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_ACTIVATE_CONTRACT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_RECALCULATE_CONTRACT;

@Component
public class ContractRecalculation {
    @Autowired
    DataUtils dataUtils;
    @Autowired
    ContractUtils contractUtils;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MessageSender messageSender;
    @Autowired
    SmsUtils smsUtils;

    ContractDeductionUtils deductionUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = QUEUE_RECALCULATE_CONTRACT)
    public void recalculateContract(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            JSONObject jsonObject = new JSONObject(jsonObjectString);
            log.info("Recalculate contract request received! "+jsonObject.toString());
            Long contractId = jsonObject.getLong("contractId");
            ContractRecalculationWorker worker = new ContractRecalculationWorker(contractId);
            worker.run();
        } catch (Exception e) {
            log.error("Error recalculation: "+e,e);
        }
    }

    private class ContractRecalculationWorker  {
        private final Long contractId;
        private final DecimalFormat decimalFormat;
        private final MapSqlParameterSource sqlParams;

        public ContractRecalculationWorker(Long contractId) {
            this.contractId = contractId;
            decimalFormat = Utils.getNumberFormat();
            sqlParams = new MapSqlParameterSource();
            sqlParams.addValue("contractId", contractId);

        }

        public void run() {

            try {
                deductionUtils = new ContractDeductionUtils(contractId, dataUtils, messageSender, smsUtils);
                deductionUtils.run();
            } catch (Exception e) {
                log.error("Error running deduction! "+e,e);
            }

            log.info("Processing recalculation ID#"+contractId);
            TransactionTemplate trxTemplate = new TransactionTemplate(dataUtils.getTrx());
            trxTemplate.execute(new TransactionCallbackWithoutResult() {
                protected void doInTransactionWithoutResult(TransactionStatus status) {
                    try {
                        //calcDetail();
                        calcDeduction();
                        addDetailFromContract(SUMMARY_CONTRACT_ID, "contract_id",  String.class);
                        addDetailFromContract(SUMMARY_SIGNED_ON, "ins_on",  Date.class);
                        addDetailFromContract(SUMMARY_ACTIVE_ON, "contract_start_on",  Date.class);
                        addDetailFromContract(SUMMARY_DEDUCTION_STARTS_ON, "contract_charged_on",  Date.class);
                        addDetailFromContract(SUMMARY_DEDUCTION_ENDS_ON, "contract_end_on",  Date.class);
                        addDetailFromContract(SUMMARY_TERM, "term_approved",  String.class);
                        addDetailFromContract(SUMMARY_TOTAL_REPAYMENT, "total_repayment",  Integer.class);
                        addDetailFromContract(SUMMARY_TOTAL_BALANCE, "debt_remaining",  Integer.class);
                        addDetailFromContract(SUMMARY_TODAY_PAYABLE, "payable",  Integer.class);

                        calcPrincipal();
                        calcDailyDeduction();
                        calcTotalDebt();
                        //calcTotalRepayment();
                    }
                    catch (Exception e) {
                        log.error("Error processing contract#"+contractId+", error: "+e);
                        status.setRollbackOnly();
                    }
                }
            });
        }

        public void calcDeduction() {
            log.info("Calculate deduction ...");
        }

        private void addDetailFromContract(int itemId, String fieldName, Type dataType) {
            sqlParams.addValue("itemId", itemId);

            String sql = " INSERT INTO contract_summary (contract_id, item_id,value_formatted) (SELECT contract_id,:itemId, ";

            String format;

            if (dataType.equals(Integer.class)) format = " FORMAT(`"+fieldName+"`,0,'id_ID')  ";
            else if (dataType.equals(Date.class)) format = " DATE_FORMAT(`"+fieldName+"`, '%d %b %Y')  ";
            else format = " `"+fieldName+"` ";
            sql+= format+" FROM contract WHERE contract_id=:contractId) ON DUPLICATE KEY UPDATE value_formatted= (SELECT "+format+" FROM contract WHERE contract_id=:contractId)  ";

            dataUtils.getJT().update(sql, sqlParams);
        }

        private void calcPrincipal() throws Exception {
            log.info("Processing principal ...");
            String sql = " SELECT IFNULL(amount_approved,0) FROM contract WHERE contract_id=:contractId ";
            Long val = dataUtils.getJT().queryForObject(sql, sqlParams, Long.class);
            simpleUpdate(SUMMARY_PRINCIPAL, val);
        }

        private void calcDailyDeduction() throws Exception {
            log.info("Processing daily deduction ...");
            String sql = " SELECT IFNULL(daily_charge,0) FROM contract WHERE contract_id=:contractId ";
            Long val = dataUtils.getJT().queryForObject(sql, sqlParams, Long.class);
            simpleUpdate(SUMMARY_DAILY_DEDUCATION, val);
        }

        private void calcTotalDebt() throws Exception {
            log.info("Processing total debt ...");
            String sql = " SELECT IFNULL(daily_charge*term_approved,0) FROM contract WHERE contract_id=:contractId ";
            Long val = dataUtils.getJT().queryForObject(sql, sqlParams, Long.class);
            simpleUpdate(SUMMARY_TOTAL_DEBT, val);
        }

        private void calcTotalRepayment() throws Exception {
            log.info("Processing total repayment ...");
            String sql = " SELECT IFNULL(SUM(repayment),0) FROM contract_deduction WHERE contract_id=:contractId ";
            Long val = dataUtils.getJT().queryForObject(sql, sqlParams, Long.class);
            simpleUpdate(SUMMARY_TOTAL_REPAYMENT, val);
        }

        private void calcDebtBalance() throws Exception {
            log.info("Processing debt balance ...");

        }

        private void calcTodayPayable() throws Exception {
            log.info("Processing today payable ...");
            LocalDate today = LocalDate.now();
            sqlParams.addValue("deduct_on", today.toString("yyyy-MM-dd"));


        }

        private void simpleUpdate(int itemId, Long valueActual) {
            sqlParams.addValue("itemId", itemId);
            sqlParams.addValue("valueActual", valueActual);
            sqlParams.addValue("valueFormatted", decimalFormat.format(valueActual));

            String sql = " INSERT INTO contract_summary (contract_id,item_id,value_actual,value_formatted) VALUES " +
                    "  (:contractId,:itemId,:valueActual,:valueFormatted) ON DUPLICATE KEY UPDATE value_actual=:valueActual, value_formatted=:valueFormatted, mod_on=NOW() ";
            dataUtils.getJT().update(sql, sqlParams);
        }
    }
}
