package id.fineasy.crm.contract.worker;

import id.fineasy.crm.contract.component.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.tika.Tika;
import org.apache.tika.utils.ExceptionUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.jms.Session;
import java.io.File;
import java.io.FileInputStream;
import java.lang.invoke.MethodHandles;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static id.fineasy.crm.contract.component.DataUtils.*;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_ACTIVATE_CONTRACT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_NEW_CONTRACT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_RECALCULATE_CONTRACT;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Component
public class ContractActivation {
    @Autowired
    ApplicationContext mContext;
    @Autowired
    DataUtils dU;
    @Autowired
    ContractUtils contractUtils;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    DocumentUtils documentUtils;
    @Autowired
    ConfigUtils configUtils;
    @Autowired
    Mailer mailer;
    @Autowired
    SmsUtils smsUtils;
    @Autowired
    MessageSender messageSender;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = QUEUE_ACTIVATE_CONTRACT)
    public void newContract(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            JSONObject jsonObject = new JSONObject(jsonObjectString);

            Long contractId = jsonObject.getLong("contractId");
            Long activateBy = jsonObject.getLong("activateBy");
            ContractActivationWorker worker = new ContractActivationWorker(jsonObject);
            worker.run();
        } catch (Exception e) {
            log.error("Error start async contract activation: "+e,e);
        }
    }

    private class ContractActivationWorker {
        private final Long contractId;
        private final Long activateBy;
        private JSONObject contractDetail;
        private final JSONObject reqParams;
        boolean activationSuccess = false;
        private final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

        private ContractActivationWorker(JSONObject reqParams) throws Exception {
            this.contractId = reqParams.getLong("contractId");
            this.activateBy = reqParams.getLong("activateBy");
            contractDetail = dU.getContractDetail(contractId);
            this.reqParams = reqParams;
        }

        public void run() {
            activationSuccess = false;
            TransactionTemplate transactionTemplate = new TransactionTemplate(dU.getTrx());
            transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_SERIALIZABLE);
            transactionTemplate.execute(new TransactionCallbackWithoutResult(){
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    try {
                        log.info("Start activating contract #"+contractId);
                        contractActivation(reqParams);
                        //transactionStatus.setRollbackOnly();
                        activationSuccess = true;
                    } catch (Exception e) {
                        log.error("Error while activating contract #"+contractId+", error: "+e);
                        transactionStatus.setRollbackOnly();
                        Utils.sendError(mContext, "Error while activating contract #"+contractId, e);
                    }
                }
            });
            if (activationSuccess) {
                messageSender.send(QUEUE_RECALCULATE_CONTRACT, new JSONObject().put("contractId", contractId));
            }
        }

        ////////
        public void contractActivation(JSONObject reqParams) throws Exception {
            Long contractId = reqParams.getLong("contractId");
            Long activateBy = reqParams.getLong("activateBy");

            checkIfContractIsValidForActivation();
            updateContractStatus(STATUS_CONTRACT_BEING_ACTIVATED);
            updateActivation(activateBy);
            File contractFile = documentUtils.createPrintableDoc(contractId);
            storeContractFile(contractFile, "contract-customer");
            sendSmsDisbursement();
            //createDeductionSchedule();
            log.info("Contract is activated!");
        }

        private void updateActivation(Long activateBy) throws Exception {
            MapSqlParameterSource params = dU.newSqlParams();
            params.addValue("contractId", contractId);
            params.addValue("activateBy", activateBy);
            params.addValue("statusId", STATUS_CONTRACT_ACTIVE);
            String sql = " UPDATE contract SET activate_on=NOW()" +
                    ", contract_start_on=CURRENT_DATE()" +
                    ", contract_charged_on=ADDDATE(CURRENT_DATE(), INTERVAL 1 DAY)" +
                    ", contract_end_on=ADDDATE(CURRENT_DATE(), INTERVAL term_approved DAY)" +
                    ", mod_by = :activateBy "+
                    ", activate_by=:activateBy "+
                    ", status_id=:statusId ";
            sql+= " WHERE contract_id=:contractId ";
            dU.getJT().update(sql, params);

            params.addValue("cashflowStatusId", STATUS_CASHFLOW_COMPLETED);
            sql = " UPDATE cashflow SET trx_date=CURRENT_DATE(),status_id=:cashflowStatusId WHERE id=(SELECT disburse_id FROM contract WHERE contract_id=:contractId) ";
            dU.getJT().update(sql, params);
        }

        private void checkIfContractIsValidForActivation() throws Exception {
            MapSqlParameterSource params = dU.newSqlParams();
            params.addValue("contractId", contractId);
            params.addValue("statusId", STATUS_CONTRACT_SIGNED);
            int count = dU.getJT().queryForObject("SELECT count(*) FROM contract WHERE contract_id=:contractId and status_id=:statusId ", params, Integer.class);
            if (count <=0) throw new Exception("Contract #"+contractId+" is not valid for activation!");
            log.info("Contract #"+contractId+" is valid for activation!");
        }

        public void updateContractStatus(int statusId) throws Exception {
            MapSqlParameterSource params = dU.newSqlParams();
            params.addValue("contractId", contractId);
            params.addValue("statusId", statusId);
            dU.getJT().update("UPDATE contract SET status_id=:statusId WHERE contract_id=:contractId", params);
            updateContractDetail();
        }

        @Test
        public String getMime(File srcFile) {
            String mimeType = "application/pdf";
            try {
                Tika tika = new Tika();
                mimeType = tika.detect(srcFile);
                assertEquals(mimeType, "application/pdf");
            } catch (Exception e) {
                log.error("Unknown error: "+e);
            }
            return mimeType;
        }

        private void storeContractFile(File contractFile, String tag) throws Exception {
            updateContractDetail();
            MapSqlParameterSource params = dU.newSqlParams();
            FileInputStream in = new FileInputStream(contractFile);
            byte[] fileBytes = IOUtils.toByteArray(in);
            in.close();

            params.addValue("contractId", contractDetail.getLong("contract_id"));
            params.addValue("name", contractFile.getName());
            params.addValue("mime", getMime(contractFile));
            params.addValue("tag", tag);


            SqlLobValue fileData = new SqlLobValue(fileBytes, new DefaultLobHandler());
            params.addValue("content", fileData, Types.BLOB);

            String sql = " INSERT INTO contract_files (contract_id,name,mime,content,tag) VALUES (:contractId,:name,:mime,:content,:tag) ON DUPLICATE KEY UPDATE mod_on=NOW(), content=:content, mime=:mime ";
            dU.getJT().update(sql, params);
            log.info("File stored: "+contractFile.getName()+", type: "+getMime(contractFile));

            try {
                sendContract();
            } catch (Exception e) {
                log.error("Error while sending email: "+e);
                Utils.sendError(mContext, "Error sending email contract #"+contractDetail.getLong("contract_id"), e);
                throw e;
            }
        }

        private void sendContract() throws Exception {
            updateContractDetail();
            if (!configUtils.getConfig("mailer.enabled").equals("1")) {
                log.warn("Mailer is disabled! Not sending email!");
                return;
            }

            String to = contractDetail.getString("full_name")+" <"+contractDetail.getString("email")+">";

            Map<String,Object> textParams = dU.contractToTextParams(contractDetail);
            StrSubstitutor substitutor = new StrSubstitutor(textParams);

            String subject = substitutor.replace(configUtils.getConfig("contract.mail.subject"));
            String content = substitutor.replace(configUtils.getConfig("contract.mail.body"));

            MapSqlParameterSource params = dU.newSqlParams();
            params.addValue("contractId", contractId);

            String sql = " SELECT * FROM contract_files WHERE contract_id=:contractId ";
            LobHandler lobHandler = new DefaultLobHandler();

            List<Mailer.FileAttachment> attachments = dU.getJT().query(sql, params, new RowMapper<Mailer.FileAttachment>() {
                @Override
                public Mailer.FileAttachment mapRow(ResultSet rs, int rowNum) throws SQLException {
                    byte[] fileBytes = lobHandler.getBlobAsBytes(rs, "content");
                    ByteArrayResource fileStream = new ByteArrayResource(fileBytes);
                    return new Mailer.FileAttachment(rs.getString("name"), fileStream, rs.getString("mime"));
                }
            });
            mailer.send(to,subject,content, attachments);
        }

        private void sendSmsDisbursement() {
            try {
                updateContractDetail();
                Map<String,Object> textParams = dU.contractToTextParams(contractDetail);
                smsUtils.send("APRROVAL_DISBURSEMENT", contractDetail.getString("mobile"), textParams, contractDetail.getString("ref"));
            } catch (Exception e) {
                log.error("Error sending SMS: "+e);
            }
        }

        private void updateContractDetail() throws Exception {
            this.contractDetail = dU.getContractDetail(contractId);
        }

        private void createDeductionSchedule() throws Exception {
            updateContractDetail();
            MapSqlParameterSource params = dU.newSqlParams();
            params.addValue("contractId", contractId);

            String sql = " DELETE FROM contract_deduction WHERE contract_id=:contractId ";
            dU.getJT().update(sql, params);

            Long term = contractDetail.getLong("term_approved");
            for (int i=0;i<term;i++) {
                params.addValue("day", i);
                params.addValue("seq", i+1);
                sql = " INSERT INTO contract_deduction (contract_id,deduct_on,amount,seq) SELECT contract_id,ADDDATE(contract_charged_on, INTERVAL :day DAY),daily_charge,:seq FROM contract WHERE contract_id=:contractId ";
                dU.getJT().update(sql, params);
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("contractId", contractId);

        }
    }
}
