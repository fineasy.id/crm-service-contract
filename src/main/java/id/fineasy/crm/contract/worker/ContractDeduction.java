package id.fineasy.crm.contract.worker;

import id.fineasy.crm.contract.component.ConfigUtils;
import id.fineasy.crm.contract.component.ContractUtils;
import id.fineasy.crm.contract.component.DataUtils;
import id.fineasy.crm.contract.component.SmsUtils;
import org.apache.tika.utils.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static id.fineasy.crm.contract.component.DataUtils.*;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_ACTIVATE_CONTRACT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_DEDUCTION_CONTRACT;

@Component
public class ContractDeduction {
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    DataSourceTransactionManager trxManager;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    SmsUtils smsUtils;
    @Autowired
    ConfigUtils configUtils;
    @Autowired
    ContractUtils contractUtils;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = QUEUE_DEDUCTION_CONTRACT)
    public void deduction(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            JSONObject jsonObject = new JSONObject(jsonObjectString);
            Long contractId = jsonObject.getLong("contractId");
            //asyncExecutor.execute(new ContractDeductionWorker(contractId));
        } catch (Exception e) {
            log.error("Error start async contract activation: "+e,e);
        }
    }

}
