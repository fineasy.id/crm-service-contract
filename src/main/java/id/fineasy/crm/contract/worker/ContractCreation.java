package id.fineasy.crm.contract.worker;

import id.fineasy.crm.contract.component.*;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.jms.Session;
import java.io.*;
import java.lang.invoke.MethodHandles;
import java.util.*;

import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_EASYPAY_DISBURSEMENT;
import static id.fineasy.crm.contract.config.ActiveMQConfig.QUEUE_NEW_CONTRACT;

@Component
public class ContractCreation {
    @Autowired
    ApplicationContext mContext;
    @Autowired
    NamedParameterJdbcTemplate namedJdbcTemplate;
    @Autowired
    DataUtils dataUtils;
    @Autowired
    DataSourceTransactionManager trxManager;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    SmsUtils smsUtils;
    @Autowired
    ConfigUtils configUtils;
    @Autowired
    ContractUtils contractUtils;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    MessageSender messageSender;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @JmsListener(destination = QUEUE_NEW_CONTRACT)
    public void newContract(@Payload String jsonString, @Headers MessageHeaders headers, Message message, Session session) {
        log.info("Received new contract request: "+jsonString);

        try {
            JSONObject jsonReq = new JSONObject(jsonString);
            Long appId = jsonReq.getLong("appId");
            ContractCreatorWorker worker = new ContractCreatorWorker(appId);
            worker.run();
        } catch (Exception e) {
            log.error("Error processing application: "+e, e);
        }
    }

    private class ContractCreatorWorker {
        private final Long appId;
        private Long contractId;
        private Long disburseId;
        private JSONObject appDetail;
        private JSONObject contractDetail;

        private ContractCreatorWorker(Long appId) {
            this.appId = appId;
            log.info("Processing approved application: "+appId);
        }

        public void run() {

            TransactionTemplate trxTemplate = new TransactionTemplate(trxManager);
            trxTemplate.execute(new TransactionCallbackWithoutResult() {
                protected void doInTransactionWithoutResult(TransactionStatus status) {
                    try {
                        getApprovedApplication();
                        createNewContract();
                        initCashFlow();
                        sendForDisbursement();
                    }
                    catch (Exception e) {
                        log.error("Error processing application#"+appId+", error: "+e);
                        status.setRollbackOnly();
                        Utils.sendError(mContext,"Error while creating contract for application #"+appId,e);
                    }
                }
            });
        }

        private void sendForDisbursement() {
            Date startDisburse = DateTime.now().plus(30000).toDate();
            log.info("Sending disbursement on: "+startDisburse);
            taskScheduler.schedule(new Runnable() {
                @Override
                public void run() {
                    log.info("Sending now for disbursement ...");
                    JSONObject jsonReq = new JSONObject();
                    jsonReq.put("contractId", contractId);
                    messageSender.send(QUEUE_EASYPAY_DISBURSEMENT, jsonReq);
                }
            }, startDisburse);
        }

        private void getApprovedApplication() throws Exception {
            MapSqlParameterSource params = dataUtils.newSqlParams();
            params.addValue("appId", appId);
            params.addValue("statusId", dataUtils.STATUS_APPROVED);
            String sql = " SELECT * FROM _application WHERE app_id=:appId AND status_id=:statusId AND contract_id IS NULL AND app_id NOT IN (SELECT app_id FROM contract)";
            List<Map<String,Object>> qr = namedJdbcTemplate.queryForList(sql, params);
            if (qr.size()<=0) throw new Exception("Valid application with #"+appId+" not found");
            appDetail = new JSONObject(qr.get(0));
            log.debug("Application detail: "+appDetail.toString());
        }

        private void createNewContract() throws Exception {
            MapSqlParameterSource params = dataUtils.newSqlParams();
            params.addValue("appId", appId);
            params.addValue("statusId", dataUtils.STATUS_CONTRACT_SIGNED);


            String sql = " INSERT INTO contract (app_id,ins_by,mod_by,status_id,amount_approved,term_approved,daily_charge,contract_start_on,contract_end_on) "+
                    " SELECT app_id,ins_by,mod_by,:statusId,amount_approved,term_approved,daily_charge,contract_start_on,contract_end_on FROM _application WHERE app_id=:appId ";

            KeyHolder holder = new GeneratedKeyHolder();
            namedJdbcTemplate.update(sql, params, holder);
            contractId = holder.getKey().longValue();
            log.info("Contract is created with ID#"+contractId);
            params.addValue("contractId", contractId);

            sql = " UPDATE application SET contract_id=:contractId WHERE app_id=:appId ";
            namedJdbcTemplate.update(sql, params);

            sql = " SELECT * FROM _contract WHERE contract_id=:contractId ";
            Map<String,Object> qr = namedJdbcTemplate.queryForMap(sql, params);
            contractDetail = new JSONObject(qr);

            Map<String,Object> messageParams = new HashMap<>();
            messageParams.put("easypay_id", appDetail.getString("easypay_id"));
            smsUtils.send("APPROVAL", appDetail.getString("mobile"), messageParams, contractDetail.getString("ref"));
        }

        private void sendPrintableDoc(Long contractId) throws Exception {
            JSONObject contractDetail = dataUtils.getContractDetail(contractId);
        }

        private void initCashFlow() {
            MapSqlParameterSource params = dataUtils.newSqlParams();
            params.addValue("contractId", contractId);
            params.addValue("typeId", dataUtils.TYPE_CASHFLOW_OUTFLOW);
            params.addValue("categoryId", dataUtils.CATEGORY_CASHFLOW_DISBURSEMENT);
            params.addValue("statusId", dataUtils.STATUS_CASHFLOW_PENDING);

            String sql = " INSERT INTO cashflow (trx_date,ins_by,mod_by,contract_id,type_id,category_id,status_id,amount) ";
            sql+= " SELECT CURRENT_DATE(),ins_by,mod_by,contract_id,:typeId,:categoryId,:statusId,amount_approved FROM contract WHERE contract_id=:contractId ";

            KeyHolder holder = new GeneratedKeyHolder();
            dataUtils.getJT().update(sql, params, holder);
            disburseId = holder.getKey().longValue();

            params.addValue("disburseId", disburseId);
            sql = " UPDATE contract SET disburse_id=:disburseId WHERE contract_id=:contractId ";
            dataUtils.getJT().update(sql, params);
        }
    }
}
